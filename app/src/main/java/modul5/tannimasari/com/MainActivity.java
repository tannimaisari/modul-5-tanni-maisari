package modul5.tannimasari.com;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvSalam;
    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNightMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvSalam = findViewById(R.id.tvSalam);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNightMode = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNightMode.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) tvSalam.setText("Good Night");
    }

    public void intentMenu (View view) {
        switch (view.getId()){
            case R.id.cardSetting:
                Intent intent1 = new Intent(MainActivity.this,SettingActivity.class);
                startActivity(intent1);
                break;
            case R.id.cardCreate:
                Intent intent2 = new Intent(MainActivity.this,AddArticle.class);
                startActivity(intent2);
                break;
            case R.id.cardList:
                Intent intent3 = new Intent(MainActivity.this,ListArticle.class);
                startActivity(intent3);
                break;
        }
    }
}
