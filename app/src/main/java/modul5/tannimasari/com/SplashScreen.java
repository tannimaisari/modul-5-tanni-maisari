package modul5.tannimasari.com;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

public class SplashScreen extends AppCompatActivity {

    ProgressBar pbSplashh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        pbSplashh = findViewById(R.id.pbSplash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        //intent setelah splash screen menuju Login
        final Intent i = new Intent(this, MainActivity.class);
        Thread timer = new Thread() {
            public void run () {
                try {
                    pbSplashh.setVisibility(View.VISIBLE);
                    for (int progress=0; progress<100; progress+=25){
                        //splash screen selama 5000ms
                        sleep(500);
                        pbSplashh.setProgress(progress);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                    finish();
                }
            }
        };
        timer.start();
        pbSplashh.setVisibility(View.GONE);
    }
}
