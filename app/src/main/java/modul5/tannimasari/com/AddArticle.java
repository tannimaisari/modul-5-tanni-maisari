package modul5.tannimasari.com;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddArticle extends AppCompatActivity {

    //deklarasi variabel
    DatabaseHelper myDb;
    EditText titleET , isiET , authorET;
    RecyclerView recyclerView;
    MyAdapter adapter;
    Cursor cursor;
    Toast myToast;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNightMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);


        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNightMode = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNightMode.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }

        recyclerView = (RecyclerView)findViewById(R.id.rv);
        //pembuatan objek DatabseHelper baru
        myDb = new DatabaseHelper(this);
        //memanggil method getAllData untuk mengambil data yang ada
        cursor  = myDb.getAllData();

        //inisialisasi variabel terhadap id view yang sudah ada
        titleET = (EditText)findViewById(R.id.titleArticle);
        isiET = (EditText)findViewById(R.id.isiArticle);
        authorET = (EditText)findViewById(R.id.authorArticle);

        //deklarasi sharedPreference
        SharedPreferences sharedP = this.getApplicationContext().
                getSharedPreferences("Preferences", 0);

        //pembuatan objek MyAdapter baru
        adapter = new MyAdapter(cursor, this);
    }

    public void addArticle (View view){
        boolean isInserted;
        //deklarasi variabel penampung isi dari edit text name, desc, dan prio
        String articleTitle = titleET.getText().toString();
        String articleIsi = isiET.getText().toString();
        String articleAuthor = authorET.getText().toString();
        if(myToast!=null){
            myToast.cancel();
        }
        //kondisi yang harus dipenuhi
        if(!articleTitle.isEmpty()&&!articleIsi.isEmpty()&&!articleAuthor.isEmpty()&&!articleAuthor.isEmpty()) {
            isInserted =  myDb.insertData(articleTitle,articleIsi,articleAuthor);
            if(!isInserted){
                //jika tidak berhasil diinput
                myToast.makeText(this,"Data tidak berhasil diinputkan",
                        Toast.LENGTH_SHORT).show();
            }else {
                //jika berhasil diinputkan
                myToast.makeText(this,"Data berhasil diinputkan",
                        Toast.LENGTH_SHORT).show();
                //memulai inten baru menuju MainActivity kembali
                startActivity(new Intent(AddArticle.this, MainActivity.class));
            }
        }else {
            //jika terdapat edit textn yang tidak diisi
            myToast.makeText(this,"Lengkapi terlebih dahulu field yang ada!",
                    Toast.LENGTH_SHORT).show();
        }
    }
//    private SimpleDateFormat getDateTime() {
//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "dd-MM-yyyy", Locale.getDefault());
//        Date date = new Date();
//        return dateFormat;
//    }
}
