package modul5.tannimasari.com;

import android.provider.BaseColumns;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Tanni on 4/7/2018.
 */


    //class yang digunakan untuk mendefinisikan skema database

public class MyDatabaseContract {

    public static final class DatabaseScheme implements BaseColumns {
        public static final String TABLE_NAME = "table_name";
        public static final String DATABASE_ID = "ID";
        public static final String TITLE ="judul";
        public static final String ISI = "isi";
        public static final String AUTHOR = "author";
//        public static final SimpleDateFormat CREATED_AT = "created_at";
    }


}
