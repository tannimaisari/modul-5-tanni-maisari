package modul5.tannimasari.com;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

public class SettingActivity extends AppCompatActivity {


    Switch swNightMode,swBigFont;
    SharedPreferences spNightMode, spBigFont;
    SharedPreferences.Editor editNight, editFont;
    Button btSave;
    TextView tvTitleMenu;

    final String PREF_NIGHT_MODE = "NightMode";
    final String PREF_FONT_SIZE = "BigSize";

    private RelativeLayout bgMenuu,bgSettingg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNightMode = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNightMode.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }

        swNightMode = findViewById(R.id.switchNightMode);
//        swNightMode.setChecked(false);
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            swNightMode.setChecked(true);
        }
        swNightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }else{
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });

        swBigFont = findViewById(R.id.switchBigFont);
        swBigFont.setChecked(false);

        btSave = findViewById(R.id.btnSaveSetting);
    }

    /**
     * Callback for activity pause.  Shared preferences are saved in this callback.
     */
    @Override
    protected void onPause() {
        super.onPause();

//        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
//        preferencesEditor.putInt(COLOR_KEY, mColor);
//        preferencesEditor.apply();
    }

    public void changeBackground(View view) {
        if (swNightMode.isChecked()){
            editNight = spNightMode.edit();
            editNight.putBoolean(PREF_NIGHT_MODE, true);
            editNight.apply();
        }

        Intent i = new Intent(SettingActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
