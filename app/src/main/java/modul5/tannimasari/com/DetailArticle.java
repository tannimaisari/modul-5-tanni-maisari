package modul5.tannimasari.com;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.widget.TextView;

public class DetailArticle extends AppCompatActivity {

    TextView judulDtl,authDtl,isiDtl;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNightMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNightMode = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNightMode.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }

        judulDtl = findViewById(R.id.titleDetail);
        authDtl = findViewById(R.id.authorDetail);
        isiDtl = findViewById(R.id.isiDetail);

        if (getIntent() != null){
            judulDtl.setText(getIntent().getStringExtra("judul"));
            authDtl.setText(getIntent().getStringExtra("penulis"));
            isiDtl.setText(getIntent().getStringExtra("deskripsi"));
        }

    }
}
