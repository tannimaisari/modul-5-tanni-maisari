package modul5.tannimasari.com;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by TanniMaisari on 4/7/2019.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    //deklarasi variabel
    private Cursor mCursor;
    private Context mContext;
//    int color;

    public MyAdapter(Cursor mCursor, Context mContext) {
        //deklarasi konstruktor yang digunakan
        this.mCursor = mCursor;
        this.mContext = mContext;
//        this.color = color;

    }

    //holder untuk recyclerview
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        //membuat view baru
        View view = inflater.inflate(R.layout.text_item,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if(!mCursor.moveToPosition(position)){
            return;
        }
        //menentukan nilai yang disimpan oleh viewholder
        String id =  mCursor.getString(mCursor.getColumnIndex(MyDatabaseContract.DatabaseScheme.DATABASE_ID));
        final String title  = mCursor.getString(mCursor.getColumnIndex(MyDatabaseContract.DatabaseScheme.TITLE));
        final String isi = mCursor.getString(mCursor.getColumnIndex(MyDatabaseContract.DatabaseScheme.ISI));
        final String author = mCursor.getString(mCursor.getColumnIndex(MyDatabaseContract.DatabaseScheme.AUTHOR));
//        final SimpleDateFormat created_at = mCursor.getString(mCursor.getColumnIndex(MyDatabaseContract.DatabaseScheme.CREATED_AT));

        holder.infoText1.setText(title);
        holder.infoText2.setText(isi);
        holder.infoText3.setText(author);

        holder.itemView.setTag(title);
        holder.itemView.setTag(isi);
        holder.itemView.setTag(author);
        holder.itemView.setTag(R.string.key,id);
        //menentukan warna berdasarkan variabel color
//        holder.cv.setCardBackgroundColor(mContext.getResources().getColor(this.color));
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,DetailArticle.class);
                intent.putExtra("judul",title);
                intent.putExtra("penulis",author);
                intent.putExtra("deskripsi",isi);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    //setiap kali ada perubahan terhadap database kita memerlukan pertukaran cursor
    public void swapCursor(Cursor newCursor) {
        //selalu menutup cursor sebelumnya terlebih dahulu
        if (mCursor != null) mCursor.close();
            mCursor = newCursor;
        if (newCursor != null) {
            // Force the RecyclerView to refresh
            //memaksai recyclerview untuk refresh data
            this.notifyDataSetChanged();
        }
    }


    class MyViewHolder extends RecyclerView.ViewHolder{
        //deklarasi variabel
        TextView infoText1,infoText2,infoText3,dtl1,dtl2,dtl3;
        CardView cv;

        public MyViewHolder(View itemView) {
            super(itemView);
            //inisialisasi variabel terhadap id yang sesuai
            infoText1 = (TextView)itemView.findViewById(R.id.articleAuthor);
            infoText2 = (TextView)itemView.findViewById(R.id.articleTitle);
            infoText3 = (TextView)itemView.findViewById(R.id.articleIsi);
            dtl1 = (TextView) itemView.findViewById(R.id.titleDetail);
            dtl2 = (TextView) itemView.findViewById(R.id.authorDetail);
            dtl3 = (TextView) itemView.findViewById(R.id.isiDetail);
            cv = (CardView) itemView.findViewById(R.id.listArticle);
        }
    }
}
