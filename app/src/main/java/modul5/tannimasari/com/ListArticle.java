package modul5.tannimasari.com;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

public class ListArticle extends AppCompatActivity {

    DatabaseHelper myDb;
    RecyclerView recyclerView;
    Cursor cursor;
    MyAdapter adapter;
    CardView cv;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNightMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);


        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNightMode = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNightMode.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }

        //deklarasi view yang digunakan
        recyclerView = (RecyclerView)findViewById(R.id.rv);
        myDb = new DatabaseHelper(this);
        cursor  = myDb.getAllData();
        cv = findViewById(R.id.listArticle);

        adapter = new MyAdapter(cursor,this);

        //menginisialisasi shared preference
        SharedPreferences sharedP = this.getApplicationContext().getSharedPreferences("Preferences", 0);

        //2 baris dibawah digunakan untuk mengambil data yang telah diinputkan
        //lalu di masukkan menggunakan adapter
        adapter.swapCursor(myDb.getAllData());
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //arah dari swipe yang digunakan untuk menghapus data adalah arak kiri dan kanan
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT| ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                String id = (String)viewHolder.itemView.getTag(R.string.key);
                //memanggil method deleteDataSwiping untuk menghapus data yang ingin dihapus
                myDb.deleteDataSwipping(id);
                //mengambil kembali data yang masih ada/tidak dihapus
                adapter.swapCursor(myDb.getAllData());

            }
        }).attachToRecyclerView(recyclerView);//menampilkasn pada recycler view

    }
}
